import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {deleteNote, fetchNotes} from "../../store/actions/action";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";

import NoteItem from "./NoteItem";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Notes = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const notes = useSelector(state => state.notes);
    const loading = useSelector(state => state.notesLoading);

    useEffect(() => {
        dispatch(fetchNotes());
    }, [dispatch]);

    const deleteNoteHandler = (e, id) => {
        dispatch(deleteNote(id));
    } ;

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Notes</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/notes/new-note">Add note</Button>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : notes.map(note => (
                    <NoteItem
                        key={note._id}
                        id={note._id}
                        title={note.title}
                        image={note.image}
                        deleteButton={e => deleteNoteHandler(e, note._id)}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Notes;