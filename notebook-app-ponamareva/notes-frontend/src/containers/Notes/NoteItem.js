import React from 'react';
import PropTypes from 'prop-types';
import {apiURL} from "../../config";

import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {CardMedia} from "@material-ui/core";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from '@material-ui/icons/Edit';

import imageNotAvailable from '../../assets/images/imageNotAvailable.png';

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const NoteItem = ({title, image, id, deleteButton}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardActions>
                    <IconButton component={Link} to={'/notes/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                    <IconButton onClick={deleteButton}>
                        <DeleteOutlineIcon/>
                    </IconButton>
                    <IconButton component={Link} to={'/edit-note/' + id}>
                        <EditIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};

NoteItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default NoteItem;