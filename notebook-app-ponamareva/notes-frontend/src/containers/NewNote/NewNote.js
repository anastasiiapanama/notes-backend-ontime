import React from 'react';
import {useDispatch} from "react-redux";
import {createNote} from "../../store/actions/action";

import NoteForm from "../../components/NoteForm/NoteForm";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const NewNote = ({history}) => {
    const dispatch = useDispatch();

    const onNoteFormSubmit = async noteData => {
        await  dispatch(createNote(noteData));

        history.push('/');
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new note</Typography>
            </Grid>
            <Grid item xs>
                <NoteForm onSubmit={onNoteFormSubmit}/>
            </Grid>
        </Grid>
    );
};

export default NewNote;