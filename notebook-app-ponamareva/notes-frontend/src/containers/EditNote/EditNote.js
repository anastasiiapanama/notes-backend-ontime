import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {editNote, fetchNote} from "../../store/actions/action";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import FileInput from "../../components/UI/Form/FileInput";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    },
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
}));

const EditNote = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const note = useSelector(state => state.note);
    const loading = useSelector(state => state.notesLoading);

    const [oneItem, setOneItem] = useState({});

    useEffect(() => {
        dispatch(fetchNote(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneItem(note);
    }, [note]);

    const submitFormHandler = async (e) => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(oneItem).forEach(key => {
            formData.append(key, oneItem[key]);
        });

        setOneItem(formData);

        await dispatch(editNote(props.match.params.id, oneItem, props.history));
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setOneItem(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setOneItem(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Edit note</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="column" alignContent="center" spacing={1}>
                {loading ? (
                        <Grid container justify="center" alignItems="center" className={classes.progress}>
                            <Grid item>
                                <CircularProgress />
                            </Grid>
                        </Grid>
                    ) :
                    <form onSubmit={submitFormHandler}>
                        <Grid container direction="column" spacing={2}>
                            <Grid item xs>
                                <TextField
                                    fullWidth
                                    variant="outlined"
                                    id="title"
                                    name="title"
                                    value={oneItem.title}
                                    onChange={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs>
                                <TextField
                                    fullWidth
                                    multiline
                                    rows={3}
                                    variant="outlined"
                                    id="content"
                                    name="content"
                                    value={oneItem.content}
                                    onChange={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs>
                                <FileInput
                                    name="image"
                                    label="Image"
                                    onChange={fileChangeHandler}
                                />
                            </Grid>
                            <Grid item xs>
                                <Button type="submit" color="primary" variant="contained">
                                    Edit
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                }
            </Grid>
        </Grid>
    );
};

export default EditNote;