import {FETCH_NOTE_SUCCESS, FETCH_NOTES_FAILURE, FETCH_NOTES_REQUEST, FETCH_NOTES_SUCCESS} from "../actions/action";

const initialState = {
    notes: [],
    note: {},
    notesLoading: false
};

const notesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NOTES_REQUEST:
            return {...state, notesLoading: true};
        case FETCH_NOTES_SUCCESS:
            return {...state, notesLoading: false, notes: action.notes};
        case FETCH_NOTE_SUCCESS:
            return {...state, note: action.note};
        case FETCH_NOTES_FAILURE:
            return {...state, notesLoading: false};
        default:
            return state;
    }
};

export default notesReducer;