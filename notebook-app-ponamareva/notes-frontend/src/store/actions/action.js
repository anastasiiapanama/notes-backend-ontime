import axiosApi from "../../axios-api";
import {NotificationManager} from "react-notifications";

export const FETCH_NOTES_REQUEST = 'FETCH_NOTES_REQUEST';
export const FETCH_NOTES_SUCCESS = 'FETCH_NOTES_SUCCESS';
export const FETCH_NOTES_FAILURE = 'FETCH_NOTES_FAILURE';

export const FETCH_NOTE_SUCCESS = 'FETCH_NOTE_SUCCESS';

export const CREATE_NOTE_SUCCESS = 'CREATE_NOTE_SUCCESS';

export const fetchNotesRequest = () => ({type: FETCH_NOTES_REQUEST});
export const fetchNotesSuccess = notes => ({type: FETCH_NOTES_SUCCESS, notes});
export const fetchNotesFailure = () => ({type: FETCH_NOTES_FAILURE});

export const fetchNoteSuccess = note => ({type: FETCH_NOTE_SUCCESS, note});

export const createNoteSuccess = () => ({type: CREATE_NOTE_SUCCESS});

export const fetchNotes = () => {
    return async dispatch => {
        try {
            dispatch(fetchNotesRequest());
            const response = await axiosApi.get('/notes');
            dispatch(fetchNotesSuccess(response.data));
        } catch (e){
            dispatch(fetchNotesFailure());
            NotificationManager.error('Could not fetch notes');
        }
    };
};

export const deleteNote = id => {
    return async dispatch => {
        try {
            await axiosApi.delete( '/notes/' + id);

            dispatch(fetchNotes());
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchNote = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/notes/' + id);

            dispatch(fetchNoteSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const createNote = noteData => {
    return async dispatch => {
        try {
            await axiosApi.post('/notes', noteData);
            dispatch(createNoteSuccess());
            dispatch(fetchNotes());
        } catch (e) {
            console.log(e);
        }
    };
};

export const editNote = (id, noteData, history) => {
    return async dispatch => {
        try {
            await axiosApi.put(`/notes/${id}`, noteData);
            history.push('/');
        } catch (e) {
            console.log(e);
        }
    };
};