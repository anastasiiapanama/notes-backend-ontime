import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchNote} from "../../store/actions/action";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Card, CardContent} from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    },
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
}));

const Note = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const note = useSelector(state => state.note);
    const loading = useSelector(state => state.notesLoading);

    const [oneItem, setOneItem] = useState({});

    useEffect(() => {
        dispatch(fetchNote(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneItem(note);
    }, [note]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Note</Typography>
                </Grid>
            </Grid>
            <Grid item container direction="column" alignContent="center" spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) :
                    <Grid item xs={9}>
                        <Card className={classes.card}>
                            <CardHeader
                                style={{textAlign: "center"}}
                                title={oneItem.title}
                            />
                            <CardContent>
                                <p style={{marginLeft: '10px'}}>
                                    "{oneItem.content}"
                                </p>
                                <p style={{marginLeft: '10px'}}>
                                    <strong>Created:</strong> {oneItem.date}
                                </p>
                            </CardContent>
                        </Card>
                    </Grid>
                }
            </Grid>
        </Grid>
    );
};

export default Note;