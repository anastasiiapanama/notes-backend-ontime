import React from 'react';
import {Switch, Route} from "react-router-dom";

import Notes from "./containers/Notes/Notes";
import Note from "./components/Note/Note";
import NewNote from "./containers/NewNote/NewNote";
import EditNote from "./containers/EditNote/EditNote";

import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Container from "@material-ui/core/Container";

const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Notes}/>
            <Route path="/notes/new-note" exact component={NewNote}/>
            <Route path="/notes/:id" exact component={Note}/>
            <Route path="/edit-note/:id" exact component={EditNote}/>
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Container>
      </main>
    </>
);

export default App;