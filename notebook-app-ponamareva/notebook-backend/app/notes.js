const express = require('express');
const path = require('path');
const multer = require('multer');

const Note = require("../models/Notes");

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const notes = await Note.find(); // mongoose

        res.send(notes);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const note = await Note.findOne({_id: req.params.id});

        if (note) {
            res.send(note)
        } else (
            res.sendStatus(404)
        )

    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const note = await Note.findOne({_id: req.params.id}).remove();

        res.send(note);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const noteRequestData = req.body;

        if (req.file) {
            noteRequestData.image = req.file.filename;
        }

        const note = new Note(noteRequestData);
        await note.save();
        res.send(note);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.put('/:id', async (req, res) => {
    try {
        const noteRequestData = req.body;

        if (req.file) {
            noteRequestData.image = req.file.filename;
        }

        const result = await Note.findById({_id: req.params.id}).update(noteRequestData);

        res.send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;